export function createCard(title, description, pictureUrl, startDate, endDate, locationInfo) {
  const card = document.createElement('div');
  card.setAttribute('class', 'card shadow-lg p-3 mb-5 bg-white rounded');
  card.innerHTML = `
    <img src='${pictureUrl}' class='card-img-top' alt='${title}'>
    <div class='card-body'>
      <div class='card-body-content'>
      <h5 class='card-title text-center'>${title}</h5>
      <p class='card-text text-center'><small class='text-muted'>${locationInfo}</small></p>
      <p class='card-text text-center'>${description}</p>
      </div>
      <div class="card-footer text-center">${startDate}-${endDate}</div>
    </div>
  `;
  return card;
}
export function createAlert(error) {
  alert = document.createElement('div')
  alert.innerHTML =`
  <div class="alert alert-danger d-flex align-items-center" role="alert">
  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
  <div>${error}</div>
  </div>`;
  return alert;
}



window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Error!')
    } else {
      const data = await response.json();
      let i = 1
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8001${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();
          const locationInfo = details.conference.location.name
          const html = createCard(title, description, pictureUrl, startDate, endDate, locationInfo);
          const column = document.querySelector(`#col-${i % 3}`);
          i += 1
          column.appendChild(html);
        }
      }

    }
  } catch (e) {
    const errorMessage = (`${e.name}: ${e.message}`)
    const alertSystem = createAlert(errorMessage)
    const main = document.querySelector('main')
    main.appendChild(alertSystem)
  }

});
